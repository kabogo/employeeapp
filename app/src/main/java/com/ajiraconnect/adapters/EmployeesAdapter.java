package com.ajiraconnect.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ajiraconnect.R;
import com.ajiraconnect.callbacks.EmployeeClickListener;
import com.ajiraconnect.dao.Employee;

import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class EmployeesAdapter extends RecyclerView.Adapter<EmployeesAdapter.EmployeeViewHolder> {
    private Context context;
    private List<Employee> employees;
    private EmployeeClickListener listener;


    public EmployeesAdapter(Context context, List<Employee> employees, EmployeeClickListener listener) {
        this.context = context;
        this.employees = employees;
        this.listener = listener;
    }

    @NonNull
    @Override
    public EmployeeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new EmployeeViewHolder(LayoutInflater.from(context).inflate(R.layout.employee_view,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull EmployeeViewHolder holder, int position) {
        int[] colors={R.drawable.circle_bg_blue,
                R.drawable.circle_bg_green,R.drawable.circle_bg_red,R.drawable.circle_bg_orange};
        Employee employee=employees.get(position);
        holder.name.setText(employee.getFullName());
        holder.departmentInitial.setText(employee.getDepartmentInitial());
        holder.division.setText(employee.getDivision());
        Random r=new Random();
        int bg = r.nextInt(3);
        holder.avatarView.setBackground(context.getResources().getDrawable(colors[bg]));
        String name=employee.getFullName();
        String initial=name.substring(0,1);
        if(name.contains(",")){
            initial=initial+name.substring(name.indexOf(",")+1,name.indexOf(",")+3).trim();
        }
        holder.initial.setText(initial);
    }

    @Override
    public int getItemCount() {
        return employees.size();
    }

    public class EmployeeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.department_init)
        TextView departmentInitial;
        @BindView(R.id.division)
        TextView division;
        @BindView(R.id.avatar)
        FrameLayout avatarView;
        @BindView(R.id.initial)
        TextView initial;

        public EmployeeViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.onEmployeeClicked(getAdapterPosition());
        }
    }
}
