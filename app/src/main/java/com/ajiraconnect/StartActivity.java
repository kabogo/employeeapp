package com.ajiraconnect;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class StartActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    FirebaseUser currentUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        new Handler().postDelayed(this::startMain,3000);
    }
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
      currentUser = mAuth.getCurrentUser();
    }

    private void startMain() {
        //check if user is logged in
        //if not take them to login
        Intent intent;
        if(currentUser!=null){
            intent=new Intent(this,MainActivity.class);
        }else {
            intent=new Intent(this,Login.class);
        }
        startActivity(intent);
        finish();
    }
}
