package com.ajiraconnect.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

@Dao
public interface UserDao {
    @Insert
    void insert(User user);

    @Update
    void update(User user);

    @Query("SELECT * FROM user WHERE id=:id")
    Single<User> findById(String id);

    @Query("SELECT * FROM user WHERE token=:token")
    Single<User> findByToken(String token);

    @Delete
    void delete(User user);

    @Query("SELECT * FROM user")
    Observable<List<User>> findAll();
}
