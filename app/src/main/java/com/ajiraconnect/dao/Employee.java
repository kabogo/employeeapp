package com.ajiraconnect.dao;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.time.Instant;
import java.util.Date;

import lombok.Data;

@Data
public class Employee implements Serializable {
    private String division;
    @SerializedName("assignment_category")
    private String assignmentCategory;
    @SerializedName("full_name")
    private String fullName;
    private String gender;
    @SerializedName("date_first_hired")
    private String dateFirstHired;
    @SerializedName("_2014_gross_pay_received")
    private double grossPayReceived;
    @SerializedName("_2014_overtime_pay")
    private double overtimePay;
    @SerializedName("current_annual_salary")
    private double annualSalary;
    @SerializedName("department_name")
    private String departmentName;
    @SerializedName("position_title")
    private String title;
    @SerializedName("dept")
    private String departmentInitial;
}
