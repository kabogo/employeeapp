package com.ajiraconnect;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.TextView;

import com.ajiraconnect.dao.Employee;
import com.ajiraconnect.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EmployeeDetail extends AppCompatActivity {

    @BindView(R.id.back_btn)
    ImageButton backBtn;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.department)
    TextView department;
    @BindView(R.id.division)
    TextView division;
    @BindView(R.id.gender)
    TextView gender;
    @BindView(R.id.gross_pay)
    TextView grossPay;
    @BindView(R.id.overtime)
    TextView overtime;
    @BindView(R.id.annual_salary)
    TextView salary;
    @BindView(R.id.assignment)
    TextView assignment;
    @BindView(R.id.position)
    TextView position;
    Employee employee;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_detail);
        ButterKnife.bind(this);
        employee = (Employee) getIntent().getExtras().get("employee");
        updateViews();
    }

    private void updateViews() {
        name.setText(employee.getFullName());
        department.setText(employee.getDepartmentName());
        division.setText(employee.getDivision());
        gender.setText(employee.getGender().equalsIgnoreCase("F") ? "Female" : "Male");
        grossPay.setText(Utils.fomartAmmount(employee.getGrossPayReceived()));
        overtime.setText(Utils.fomartAmmount(employee.getOvertimePay()));
        salary.setText(Utils.fomartAmmount(employee.getAnnualSalary()));
        position.setText(employee.getTitle());
        assignment.setText(employee.getAssignmentCategory());
    }

    @OnClick(R.id.back_btn)
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
