package com.ajiraconnect.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class Utils {
    public static boolean hasToken(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).contains("token");
    }

    public static String getAccessToken(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("token", null);
    }

    public static void saveToken(String token, Context context) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("token", token).apply();
    }
    public static void saveId(String userid,Context context){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("id", userid).apply();

    }
    public static String getId(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("id", null);
    }

    public static void clearToken(Context context) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().remove("token").apply();
    }

    public static String formatAmount(long amount) {
        String formattedPrice = String.valueOf(amount);
        if (amount < 2147483647L) {
            formattedPrice = NumberFormat.getNumberInstance(Locale.US).format(amount);
        }
        return formattedPrice;
    }

    public static String fomartAmmount(double amount){
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.getDefault());
        DecimalFormat df = (DecimalFormat)nf;
        df.applyPattern("###,###.00");
        return "$ "+df.format(amount);
    }


    public static String formatAmount(String amount){
        double d=Double.parseDouble(amount);
        long l = Math.round(d);
        return formatAmount(l);
    }
    public static boolean isOnBoardingShown(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).contains("onboarding");
    }

    public static void showOnBoarding(Context context) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("onboarding", "flag").apply();
    }

    public static void clearPrefs(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove("token");
        editor.remove("onboarding");
        editor.remove("id");
        editor.apply();
    }


}
