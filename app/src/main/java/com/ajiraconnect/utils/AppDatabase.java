package com.ajiraconnect.utils;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.ajiraconnect.dao.User;
import com.ajiraconnect.dao.UserDao;


@Database(entities = {User.class}, version = 1, exportSchema = false)
@TypeConverters({MyTypeConverter.class})
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase instance;
    public static AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context, AppDatabase.class, "ajira_employees_db")
                    .fallbackToDestructiveMigration().build();
        }
        return instance;
    }

    public abstract UserDao userDao();
    //abstract access interfaces
}
