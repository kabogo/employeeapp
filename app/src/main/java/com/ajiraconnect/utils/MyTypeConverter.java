package com.ajiraconnect.utils;

import androidx.room.TypeConverter;

import java.util.Date;

public class MyTypeConverter {


    @TypeConverter
    public long dateToMill(Date date){
        return date.getTime();
    }
    @TypeConverter
    public Date milliToDate(long time){
        Date date = new Date();
        date.setTime(time);
        return date;
    }


}
