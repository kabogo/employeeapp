package com.ajiraconnect.services;

import com.ajiraconnect.dao.Employee;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface ApiService {

    @GET("54rh-89p8")
    Observable<List<Employee>> findAll(@Query("$offset") int page,@Query("$limit") int limit);
}
