package com.ajiraconnect.services;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.ajiraconnect.dao.Employee;
import com.ajiraconnect.utils.Constants;

import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.schedulers.Schedulers;

public class NetworkService {

    private static NetworkService instance;
    private ApiService apiService = null;

    public static boolean isNetworkAvailable(Context context) {
        NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    private NetworkService(Context context) {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS).build();
        GsonConverterFactory gsonConverterFactory = GsonConverterFactory.create();
        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(gsonConverterFactory)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .baseUrl(Constants.BASE_URL).build();
        apiService = retrofit.create(ApiService.class);
    }

    public static NetworkService getInstance(Context context) {
        if (instance == null) {
            instance = new NetworkService(context);
        }
        return instance;
    }

    public Observable<List<Employee>> findEmployees() {
        return apiService.findAll(0,10);
    }

    public Observable<List<Employee>> findEmployees(int page) {
        return apiService.findAll(page,10);
    }

}
