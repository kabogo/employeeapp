package com.ajiraconnect;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.Patterns;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.ajiraconnect.dao.User;
import com.ajiraconnect.services.NetworkService;
import com.ajiraconnect.utils.AppDatabase;
import com.ajiraconnect.utils.Utils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import okhttp3.internal.Util;
import rx.Completable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class Login extends AppCompatActivity {

    @BindView(R.id.login_btn)
    View loginBtn;
    @BindView(R.id.register)
    TextView register;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    Subscription subscription;
    private ACProgressFlower dialog;

    User user;
    @BindView(R.id.password_show_btn)
    ImageButton passwordShow;
    @BindView(R.id.password_hide_btn)
    ImageButton passwordHideBtn;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        //check if user is logged in
        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        checkIfLoggedIn();
    }
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser!=null){
            startMain();
        }
    }

    private void checkIfLoggedIn() {
        if (Utils.hasToken(this)) {
            startMain();
        }
    }

    @OnClick(R.id.register)
    public void register() {
        startActivity(new Intent(this, Register.class));
    }



    public void startMain() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @OnClick(R.id.login_btn)
    public void emailLogin() {
        if (TextUtils.isEmpty(email.getText().toString())) {
            Toast.makeText(this, "Username or Email required!", Toast.LENGTH_SHORT).show();
            return;
        }
        if(email.getText().toString().contains("@")){
            if (!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
                Toast.makeText(this, "E-mail address not valid!", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        if (TextUtils.isEmpty(password.getText().toString())) {
            Toast.makeText(this, "Password required!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (NetworkService.isNetworkAvailable(this)) {
            //login with firebase
            showDialog();
            mAuth.signInWithEmailAndPassword(email.getText().toString().trim(), password.getText().toString())
                    .addOnCompleteListener(this, task -> {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser firebaseUser = mAuth.getCurrentUser();
                            User user=new User();
                            user.setEmail(firebaseUser.getEmail());
                            user.setUsername(firebaseUser.getDisplayName());
                            user.setAvatar(firebaseUser.getPhotoUrl()!=null?firebaseUser.getPhotoUrl().getPath():null);
                            user.setId(firebaseUser.getUid());
                            user.setPhone(firebaseUser.getPhoneNumber());
                            saveUser(user);
                            hideDialog();
                        } else {
                            // If sign in fails, display a message to the user.
                            hideDialog();
                            Toast.makeText(getBaseContext(),task.getException().getMessage(),Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    });
        } else {
            //show network dialog
            showNetworkDialog();
        }
    }

    //save user and start main
    private void saveUser(User u) {
        Completable.fromAction(() -> {
            //save user
            //  update prefs
           Utils.saveId(u.getId(),this);
            AppDatabase.getInstance(this).userDao().insert(u);

        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnCompleted(() -> {
                    hideDialog();
                    startMain();
                }).doOnError(Throwable::printStackTrace)
                .subscribe();
    }



    private void showDialog() {
        runOnUiThread(() -> {
            if (dialog == null) {
                dialog = new ACProgressFlower.Builder(this)
                        .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                        .themeColor(Color.WHITE)
                        .textColor(Color.WHITE)
                        .bgAlpha(0)
                        .fadeColor(Color.DKGRAY).build();
                dialog.setCancelable(false);
                dialog.show();
            } else {
                if (!dialog.isShowing()) {
                    dialog.show();
                }
            }

        });

    }

    private void hideDialog() {
        runOnUiThread(() -> {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        });

    }

    private void showNetworkDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.network_dialog, null, false);
        View retry = view.findViewById(R.id.btn_retry);
        View cancel = view.findViewById(R.id.btn_cancel);
        view.setAnimation(AnimationUtils.loadAnimation(this, R.anim.scale_up));
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        retry.setOnClickListener(v -> {
            alertDialog.dismiss();
            emailLogin();
        });
        cancel.setOnClickListener(v -> alertDialog.dismiss());
    }

    @OnClick(R.id.password_show_btn)
    public void passwordShw() {
        if (passwordShow.getVisibility() != View.GONE) {
            passwordShow.setVisibility(View.GONE);
        }
        if (passwordHideBtn.getVisibility() != View.VISIBLE) {
            passwordHideBtn.setVisibility(View.VISIBLE);
        }
        password.setTransformationMethod(null);
    }

    @OnClick(R.id.password_hide_btn)
    public void passwordHide() {
        if (passwordShow.getVisibility() != View.VISIBLE) {
            passwordShow.setVisibility(View.VISIBLE);
        }
        if (passwordHideBtn.getVisibility() != View.GONE) {
            passwordHideBtn.setVisibility(View.GONE);
        }
        password.setTransformationMethod(new PasswordTransformationMethod());
    }
}
