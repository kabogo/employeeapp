package com.ajiraconnect.fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.ajiraconnect.R;
import com.ajiraconnect.dao.User;
import com.ajiraconnect.utils.AppDatabase;
import com.ajiraconnect.utils.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import rx.Completable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.avatar)
    CircleImageView avatar;
    @BindView(R.id.avatar_c)
    View avatarC;
    private User user;
    private FirebaseAuth mAuth;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        mAuth = FirebaseAuth.getInstance();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        email.setText(user.getEmail());
        name.setText(user.getUsername());
        Picasso.get().load(user.getAvatar()).error(R.drawable.ic_person_grey)
                .placeholder(R.drawable.ic_person_grey).into(avatar);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            user = (User) getArguments().getSerializable("user");
        }
    }

    @OnClick(R.id.update_btn)
    public void updateAccount() {
        if (!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
            Toast.makeText(getContext(), "Email is invalid", Toast.LENGTH_SHORT).show();
            return;
        }

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) return;

        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(name.getText().toString().trim())
                .build();
        Toast.makeText(getContext(), "Updating..", Toast.LENGTH_SHORT).show();
        user.updateProfile(profileUpdates)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        FirebaseUser firebaseUser = mAuth.getCurrentUser();
                        User u = new User();
                        u.setEmail(firebaseUser.getEmail());
                        u.setUsername(firebaseUser.getDisplayName());
                        u.setAvatar(firebaseUser.getPhotoUrl() != null ? firebaseUser.getPhotoUrl().getPath() : null);
                        u.setId(firebaseUser.getUid());
                        u.setPhone(firebaseUser.getPhoneNumber());
                        saveUser(u);
                    }
                });

    }


    private void saveUser(User u) {
        Completable.fromAction(() -> {
            AppDatabase.getInstance(getContext()).userDao().update(u);

        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnCompleted(() -> {
                    Toast.makeText(getContext(), "Profile updated", Toast.LENGTH_SHORT).show();
                }).doOnError(Throwable::printStackTrace)
                .subscribe();
    }
}
