package com.ajiraconnect.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;

import com.ajiraconnect.EmployeeDetail;
import com.ajiraconnect.R;
import com.ajiraconnect.adapters.EmployeesAdapter;
import com.ajiraconnect.callbacks.EmployeeClickListener;
import com.ajiraconnect.dao.Employee;
import com.ajiraconnect.services.NetworkService;
import com.ajiraconnect.utils.EndlessScrollListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmployeeListFragment extends Fragment implements EmployeeClickListener, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    private List<Employee> employeeList=new ArrayList<>();
    private EmployeesAdapter employeesAdapter;
    private Context context;

    private int defaultPage=0;
    private int start=0;
    private int end=0;
    private EndlessScrollListener endlessScrollListener;
    private boolean shouldNavigate=false;
    public EmployeeListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_employee_list, container, false);
        ButterKnife.bind(this,view);
        employeesAdapter=new EmployeesAdapter(getContext(),employeeList,this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(employeesAdapter);
        swipeRefreshLayout.setOnRefreshListener(this);
        context=getContext();
        endlessScrollListener=new EndlessScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                findEmployees();
            }
        };
        recyclerView.addOnScrollListener(endlessScrollListener);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findEmployees();
    }

    private void findEmployees(){
        if(NetworkService.isNetworkAvailable(context)){
            NetworkService.getInstance(context).findEmployees(defaultPage)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .doOnSubscribe(()->{
                       if(getActivity()!=null){
                           getActivity().runOnUiThread(()->{
                               if(!swipeRefreshLayout.isRefreshing()){
                                   swipeRefreshLayout.setRefreshing(true);
                               }
                           });
                       }
                    })
                    .subscribe(new Observer<List<Employee>>() {
                        @Override
                        public void onCompleted() {
                            updateUI();
                            defaultPage++;
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onNext(List<Employee> employees) {
                            start=employeeList.size();
                            end=employees.size();
                            employeeList.addAll(employees);
                        }
                    });
        }else {
            showNetworkDialog();
        }
    }

    private void updateUI() {
        employeesAdapter.notifyItemRangeInserted(start,end);
        if(swipeRefreshLayout.isRefreshing()){
            swipeRefreshLayout.setRefreshing(false);
        }
        if(shouldNavigate){
            recyclerView.scrollToPosition(start);
            shouldNavigate=false;
        }
    }

    @Override
    public void onEmployeeClicked(int position) {
        Employee employee=employeeList.get(position);
        Intent intent=new Intent(context, EmployeeDetail.class);
        intent.putExtra("employee",employee);
        startActivity(intent);
    }

    private void showNetworkDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = getLayoutInflater().inflate(R.layout.network_dialog, null, false);
        View retry = view.findViewById(R.id.btn_retry);
        View cancel = view.findViewById(R.id.btn_cancel);
        view.setAnimation(AnimationUtils.loadAnimation(context, R.anim.scale_up));
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        retry.setOnClickListener(v -> {
            alertDialog.dismiss();
         findEmployees();
        });
        cancel.setOnClickListener(v -> alertDialog.dismiss());
    }

    @Override
    public void onRefresh() {
        shouldNavigate=true;
        findEmployees();
    }
}
