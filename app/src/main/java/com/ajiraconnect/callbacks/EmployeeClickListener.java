package com.ajiraconnect.callbacks;

public interface EmployeeClickListener {
    void onEmployeeClicked(int position);
}
