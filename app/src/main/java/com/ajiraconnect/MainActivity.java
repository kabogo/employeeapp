package com.ajiraconnect;

import android.content.Intent;
import android.os.Bundle;

import com.ajiraconnect.dao.User;
import com.ajiraconnect.utils.AppDatabase;
import com.ajiraconnect.utils.Utils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.view.Gravity;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;

import android.view.Menu;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.Completable;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.internal.Util;

public class MainActivity extends AppCompatActivity implements NavController.OnDestinationChangedListener {


    @BindView(R.id.show_drawer)
    ImageButton showDrawer;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    NavController navController;
    @BindView(R.id.employees)
    TextView navEmployees;
    @BindView(R.id.profile)
    TextView navProfile;
    @BindView(R.id.share)
    TextView navShare;
    @BindView(R.id.logout)
    TextView navLogout;
    @BindView(R.id.title)
    TextView title;
    @BindString(R.string.employees)
    String employeesTitleString;
    @BindString(R.string.profile)
    String profiletitleString;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.avatar)
    CircleImageView avatar;
    private User user;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        navController.addOnDestinationChangedListener(this);
        mAuth = FirebaseAuth.getInstance();
        findUser();
        if(!Utils.isOnBoardingShown(this)){
            Toast.makeText(this,"Keep scrolling to fetch and load more employees as you scroll",Toast.LENGTH_SHORT).show();
            Utils.showOnBoarding(this);
        }
    }


    @OnClick(R.id.show_drawer)
    public void toggleDrawer() {
        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.closeDrawer(Gravity.LEFT);
        } else {
            drawerLayout.openDrawer(Gravity.LEFT);
        }

    }

    /**
     * find user from  database
     */
    private void findUser() {
        String id = Utils.getId(this);
        AppDatabase.getInstance(this).userDao().findById(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<User>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(User u) {
                        user = u;
                        updateAvatar();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
                });

    }

    private void updateAvatar() {
        email.setText(user.getEmail());
        Picasso.get().load(user.getAvatar()).error(R.drawable.ic_person_grey)
                .placeholder(R.drawable.ic_person_grey).into(avatar);
    }

    @OnClick({R.id.employees, R.id.profile, R.id.about, R.id.share, R.id.logout})
    public void onSideDrawerItemClicked(View view) {
        switch (view.getId()) {
            case R.id.employees:
                navController.navigate(R.id.to_employee_list);
                title.setText(employeesTitleString);
                break;
            case R.id.profile:
                Bundle bundle=new Bundle();
                bundle.putSerializable("user",user);
                navController.navigate(R.id.to_profile,bundle);
                title.setText(profiletitleString);
                break;
            case R.id.about:
                navController.navigate(R.id.to_help);
                title.setText("About");
                break;
            case R.id.share:
                shareAction();
                break;
            case R.id.logout:
                showAdviceDialog();
                break;
        }
        toggleDrawer();
    }


    private void shareAction() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,"Download this awesome employees management app from play store.");
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent,"Share via..."));

    }

    private void logoutUser() {
        Completable.fromAction(() -> {
            AppDatabase.getInstance(this).clearAllTables();
            Utils.clearPrefs(this);
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).doOnComplete(() -> {
            Intent intent = new Intent(this, Login.class);
            mAuth.signOut();
            startActivity(intent);
            finish();
        }).subscribe();
    }

    /**
     * ask user tp confirm logging our
     */
    private void showAdviceDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.confirm_dialog, null, false);
        View retry = view.findViewById(R.id.btn_retry);
        View cancel = view.findViewById(R.id.btn_cancel);
        view.setAnimation(AnimationUtils.loadAnimation(this, R.anim.scale_up));
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        retry.setOnClickListener(v -> {
            alertDialog.dismiss();
            logoutUser();
        });
        cancel.setOnClickListener(v -> alertDialog.dismiss());
    }

    @Override
    public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {

    }
}
