package com.ajiraconnect;

import android.view.Gravity;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityTest {
    @Rule
    public ActivityTestRule<MainActivity> activityRule
            = new ActivityTestRule<>(MainActivity.class);
    @Test
    public void toggleDrawer() {
        DrawerLayout drawerLayout = activityRule.getActivity().drawerLayout;
        drawerLayout.openDrawer(Gravity.LEFT);
        assertTrue(drawerLayout.isDrawerOpen(Gravity.LEFT));
    }

}