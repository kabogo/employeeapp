package com.ajiraconnect.services;

import android.content.Context;

import androidx.test.InstrumentationRegistry;
import androidx.test.filters.LargeTest;
import androidx.test.runner.AndroidJUnit4;

import com.ajiraconnect.dao.Employee;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import rx.Observer;
import rx.Scheduler;
import rx.schedulers.Schedulers;

import static org.junit.Assert.*;
@RunWith(AndroidJUnit4.class)
@LargeTest
public class NetworkServiceTest {

    Context context= InstrumentationRegistry.getTargetContext();
    @Test
    public void isNetworkAvailable() {
    }

    @Test
    public void getInstance() {
        NetworkService instance = NetworkService.getInstance(context);
        assertNotNull(instance);
    }

    @Test
    public void findEmployees() {
        NetworkService instance = NetworkService.getInstance(context);
        assertNotNull(instance);
        instance.findEmployees().subscribeOn(Schedulers.io()).subscribe(new Observer<List<Employee>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(List<Employee> employees) {
                assertNotNull(employees);
                assertFalse(employees.isEmpty());
            }
        });
    }

    @Test
    public void findEmployees1() {
        NetworkService instance = NetworkService.getInstance(context);
        assertNotNull(instance);
        instance.findEmployees().subscribeOn(Schedulers.io()).subscribe(new Observer<List<Employee>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(List<Employee> employees) {
                assertNotNull(employees);
                assertFalse(employees.isEmpty());
            }
        });
    }
}