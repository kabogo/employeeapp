package com.ajiraconnect;

import android.text.TextUtils;
import android.util.Patterns;
import android.widget.EditText;

import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class LoginTest {

    @Rule
    public ActivityTestRule<Login> activityRule
            = new ActivityTestRule<>(Login.class);


    @Test
    public void emailLogin() {
        EditText email = activityRule.getActivity().email;
        EditText password = activityRule.getActivity().password;
        email.setText("user@mail.com");
        password.setText("gaggsgs12");
        assertTrue(Patterns.EMAIL_ADDRESS.matcher(email.getText()).matches());
        assertTrue(!TextUtils.isEmpty(password.getText()));
    }

    @Test
    public void passwordShw() {
        assertNotNull(activityRule.getActivity().password.getTransformationMethod());
    }

    @Test
    public void passwordHide() {
        assertNotNull(activityRule.getActivity().password.getTransformationMethod());

    }
}